# Ethics

A handbook of processes and policies that help us make decisions.

## Why is this handbook important?
Ethics are the first to go when profits are on the line. The Equalify project is built around a different idea: **ethical issues are mission-critical bugs.**

## How does this handbook work?

When anyone in the organization raises an ethical issue, the issue must be resolved.

Early on, this may create slow the product delivery timeline. As issues are resolved, and processes are implemented, an ethical company develops - just like a great piece of software develops.
